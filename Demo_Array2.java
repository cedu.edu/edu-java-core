/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package demo_array2;


/**
 *
 * @author phamtuyetnga
 */
public class Demo_Array2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
    //Truy cập các phần tử có trong mảng 
        String[] name = {"Pham", "Tuyet", "Nga"};
        System.out.println(name[0]);
    //Thay đổi một phần tử mảng  
        String dayOfWeek[] = {"Monday", "Morning", "Wednesday", "Thursday", "Friday"};
        dayOfWeek[1] = "Tusday";
        System.out.println(dayOfWeek[1]);
    
    //Độ dài mảng
    int number[] = {1,2,3,4,5,6};
    System.out.println(number.length);
    
    }
    
}
