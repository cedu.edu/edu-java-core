/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package online_for_01;

import java.util.Scanner;

/**
 *
 * @author phamtuyetnga
 */
public class Online_For_01 {

    /**
     * input: nhập 1 dãy gồm n số nguyên nhập vào từ bàn phím
     * Output: tính tbc 
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter n: ");
        int n = scan.nextInt();
        int sum = 0;
        int number;
        
        for(int i = 1; i <= n; i++){
            System.out.print("Number is " + i + ":");
            number = scan.nextInt();
            sum += number;
        }
        int avgNumber = sum / n;
        System.out.println("Average is: " + avgNumber);
    }
    
}
