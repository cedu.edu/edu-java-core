/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package online_dowhile_01;

import java.util.Scanner;

/**
 *
 * @author phamtuyetnga
 */
public class Online_DoWhile_01 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner scan = new Scanner(System.in);
        int m = 0;
        int n;
        int sum = 0;
        do{
            System.out.print("Enter n: ");
            n = scan.nextInt();
        }while(n < 0 );
        while((sum + m) < n){
            sum += ++m;
            System.out.println(m);
            if((sum + m) < n){
                System.out.print(" + ");
            }
        }
        if(sum > 0){
            System.out.println("Sum is: " + sum);
        }
        System.out.println("m max = " + m);
    }
    
}
