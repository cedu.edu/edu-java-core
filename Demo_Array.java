/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package demo_array;

import java.util.Scanner;

/**
 *
 * @author phamtuyetnga
 */
public class Demo_Array {

    /**Demo khai báo , nhập và xuất mảng
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner scan = new Scanner (System.in);
        System.out.print("Please enter one-dimensional array:");
        int longs = scan.nextInt();
        int a[] = importArr(scan,longs);
        exportArr(a);
    }
    public static int[] importArr(Scanner scan, int longs){
        int a[] = new int[longs];//Khai báo mảng 
        System.out.println("Enter one-dimensional array :");
        for(int i = 0; i < a.length; i++){
            System.out.print("a["+i+"] = ");
            a[i] = scan.nextInt();
            
        }
        return a;
        
    }    
    public static void exportArr(int a[]){
        System.out.println("Import one - dimensional array:");
        for(int i = 0; i < a.length; i++){
            System.out.print(a[i] + "\t");
        }
    }
    public static void exportArrForEach(int a[]){
        System.out.print("Export one - dimesional array choose 2: ");
        for(int pt : a){
            System.out.print(pt + "\t");
        }
    }
    
}
