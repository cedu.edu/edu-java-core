/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package online_dowhile_02;

import java.util.Scanner;

/**
 *
 * @author phamtuyetnga
 */
public class Online_DoWhile_02 {

    /**
     * Input: nhập UserName, passWord 
     * Output: True = "wellcome" + userName; 
     *         False = nhập lại 
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner scan = new Scanner(System.in);
        int userName = 1409;
        int passWord = 12345;
        int guessUser;
        int guessPassWord;
        
        do{
            System.out.println("Enter useName: " );
            guessUser = scan.nextInt();
            System.out.println("Enter pass word: ");
            guessPassWord = scan.nextInt();
            
        }while((userName != guessUser ) || (passWord != guessPassWord));
         
        System.out.println("Wellcome " + guessUser);
        
    }
    
}
